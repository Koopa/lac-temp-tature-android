package com.example.lac_temperature_tp5;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


public class SaisirTemperature extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saisietemp);
        Button btnRetour = (Button)findViewById(R.id.buttonRetourAccueil);
        final Spinner spinnerHeure = (Spinner)findViewById(R.id.spinnerChoixHeure);
        String[] Heure={"4h","8h","12h","16h","20h","24h"};
        ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Heure);
        dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerHeure.setAdapter(dataAdapterR);
        View.OnClickListener ecouteurRetour = new View.OnClickListener() {
            @Override
            public void onClick(View v2) {
                switch (v2.getId()){
                    case R.id.buttonRetourAccueil:
                        Intent intent2 = new Intent(SaisirTemperature.this , MainActivity.class);
                        startActivity(intent2);
                        System.exit(0);
                        break;

                }
            }
        };
        btnRetour.setOnClickListener(ecouteurRetour);

    }

}
