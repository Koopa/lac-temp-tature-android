package com.example.lac_temperature_tp5;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DateFormat;
import java.util.Date;

public class BddTempJour extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bdd_temp_jour);



        BdAdapter releveBdd = new BdAdapter(this);


        ListView listeTempJour = (ListView) findViewById(R.id.ListeTempJour);


        //On ouvre la base de données pour écrire dedans
        releveBdd.open();
        Cursor c = releveBdd.getData();
        Toast.makeText(getApplicationContext(), "il y a " + String.valueOf(c.getCount()) + " relevés dans la BD", Toast.LENGTH_LONG).show();
        // colonnes à afficher
        String[] columns = new String[]{ BdAdapter.COL_JOUR, BdAdapter.COL_MOIS, BdAdapter.COL_HEURE, BdAdapter.COL_TEMPERATURE};

        // champs dans lesquelles afficher les colonnes
        int[] to = new int[]{R.id.textViewJour, R.id.textViewMois, R.id.textViewHeure, R.id.textViewTemp};
        SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(this, R.layout.activity_bdd_temp_jour, c, columns, to, 0);
        //Assign adapter to ListView
        listeTempJour.setAdapter(dataAdapter);
        releveBdd.close();

    }
}
